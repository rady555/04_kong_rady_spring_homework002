
-- table product
CREATE TABLE product_tb(
                       product_id SERIAL PRIMARY KEY,
                       product_name VARCHAR(200) NOT NULL,
                       product_price DECIMAL(10, 2) NOT NULL
);

-- table customer
CREATE TABLE customer_tb(
                        customer_id SERIAL PRIMARY KEY,
                        customer_name VARCHAR(200) NOT NULL,
                        customer_address VARCHAR(200) NOT NULL,
                        customer_phone VARCHAR(12) NOT NULL
);


-- table invoice
CREATE TABLE invoice_tb(
                       invoice_id SERIAL PRIMARY KEY,
                       invoice_date TIMESTAMP NOT NULL,
                       customer_id INT,
                       FOREIGN KEY (customer_id) REFERENCES customer_tb ON DELETE CASCADE ON UPDATE CASCADE
);

-- table invoice detail
CREATE TABLE invoice_detail_tb(
                      id SERIAL PRIMARY KEY,
                      invoice_id INT,
                      FOREIGN KEY (invoice_id) REFERENCES invoice_tb ON DELETE CASCADE ON UPDATE CASCADE,
                      product_id INT,
                      FOREIGN KEY (product_id) REFERENCES product_tb ON DELETE CASCADE ON UPDATE CASCADE
);
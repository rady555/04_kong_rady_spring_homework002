package com.example.product_management_system.service;

import com.example.product_management_system.model.entity.Customer;
import com.example.product_management_system.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer customerId);

    Customer insertCustomer(CustomerRequest customerRequest);

    Customer updateCustomer(Integer customerId, CustomerRequest customerRequest);

    void deleteCustomerById(Integer customerId);
}

package com.example.product_management_system.service;

import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer productId);

    Product insertProduct(ProductRequest productRequest);

    Product updateProduct(Integer productId, ProductRequest productRequest);

    void deleteProductById(Integer productId);
}

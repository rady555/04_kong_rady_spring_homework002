package com.example.product_management_system.service.impl;

import com.example.product_management_system.model.entity.Customer;
import com.example.product_management_system.model.request.CustomerRequest;
import com.example.product_management_system.repository.CustomerRepository;
import com.example.product_management_system.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public Customer insertCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertCustomer(customerRequest);
    }

    @Override
    public Customer updateCustomer(Integer customerId, CustomerRequest customerRequest) {
        return customerRepository.updateCustomer(customerId,customerRequest);
    }

    @Override
    public void deleteCustomerById(Integer customerId) {
        customerRepository.deleteCustomerById(customerId);
    }
}

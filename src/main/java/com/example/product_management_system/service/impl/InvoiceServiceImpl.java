package com.example.product_management_system.service.impl;

import com.example.product_management_system.model.entity.Invoice;
import com.example.product_management_system.model.request.InvoiceRequest;
import com.example.product_management_system.repository.InvoiceRepository;
import com.example.product_management_system.service.InvoiceService;
import org.springframework.stereotype.Service;
import java.lang.Integer;
import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice insertInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.insertInvoice(invoiceRequest);
        for (Integer productId : invoiceRequest.getProductId()) {
            invoiceRepository.insertProductIdIntoInvoiceDetail(invoiceId, productId);
        }
        return null;
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public void deleteCustomerById(Integer invoiceId) {
        invoiceRepository.deleteInvoiceById(invoiceId);
    }

    @Override
    public Invoice updateInvoice(Integer invoiceId, InvoiceRequest invoiceRequest) {
       Integer invId = invoiceRepository.updateInvoice(invoiceId,invoiceRequest);
        invoiceRepository.deleteFromInvoiceDetail(invoiceId);
        for (Integer productId : invoiceRequest.getProductId()) {
            invoiceRepository.insertToInvoiceDetail(invId, productId);
        }
        return  null;
    }
}
package com.example.product_management_system.service.impl;

import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.ProductRequest;
import com.example.product_management_system.repository.ProductRepository;
import com.example.product_management_system.service.ProductService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public Product insertProduct(ProductRequest productRequest) {
        return productRepository.insertProduct(productRequest);
    }

    @Override
    public Product updateProduct(Integer productId, ProductRequest productRequest) {
        return  productRepository.updateProduct(productId,productRequest);
    }

    @Override
    public void deleteProductById(Integer productId) {
        productRepository.deleteProduct(productId);
    }
}

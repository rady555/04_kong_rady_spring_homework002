package com.example.product_management_system.service;

import com.example.product_management_system.model.entity.Invoice;
import com.example.product_management_system.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
   List<Invoice> getAllInvoice();


   Invoice insertInvoice(InvoiceRequest invoiceRequest);

   Invoice getInvoiceById(Integer invoiceId);

   void deleteCustomerById(Integer invoiceId);


   Invoice updateInvoice(Integer invoiceId, InvoiceRequest invoiceRequest);
}

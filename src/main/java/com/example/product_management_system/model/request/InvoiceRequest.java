package com.example.product_management_system.model.request;

import com.example.product_management_system.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {
    private Timestamp invoiceDate;
    private Integer customerId;
    private List<Integer> productId;
}

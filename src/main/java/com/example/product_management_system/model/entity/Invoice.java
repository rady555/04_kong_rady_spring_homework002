package com.example.product_management_system.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Invoice {
    private Integer invoiceId;
    private Timestamp invoiceDate;
    private Customer customer;
    private List<Product> products;

}

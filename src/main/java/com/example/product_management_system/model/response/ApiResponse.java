package com.example.product_management_system.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ApiResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
//    private LocalDateTime localDateTime;
//    private HttpStatus status;
    private String message;
    private boolean success;


}

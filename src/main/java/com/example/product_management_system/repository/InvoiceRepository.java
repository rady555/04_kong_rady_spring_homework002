package com.example.product_management_system.repository;

import com.example.product_management_system.model.entity.Invoice;
import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    @Select("""
            SELECT pt.product_id,pt.product_name,pt.product_price
            FROM invoice_detail_tb idt inner join product_tb pt on idt.product_id = pt.product_id
            WHERE invoice_id = #{productId};
            """)
    @Results(id = "productMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price"),
    })
    List<Product> getProduct(Integer invoiceId);

    @Select("""
            SELECT * FROM invoice_tb
            """)
    @Results(id = "invoiceMap", value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id",
                    one = @One(select = "com.example.product_management_system.repository.CustomerRepository.getCustomerById")),
            @Result(property = "products", column = "invoice_id",
                    many = @Many(select = "getProduct"))
    })
    List<Invoice> getAllInvoice();


    @Select("""
            INSERT INTO invoice_tb ( invoice_date, customer_id)
            VALUES ( #{invoiceRequest.invoiceDate}, #{invoiceRequest.customerId})
            RETURNING invoice_id
            """)
    Integer insertInvoice(@Param("invoiceRequest") InvoiceRequest invoiceRequest);


    @Insert("""
            INSERT INTO invoice_detail_tb (invoice_id, product_id)
            VALUES (#{invoiceId}, #{productId})
            """)
    void insertProductIdIntoInvoiceDetail(Integer invoiceId, Integer productId);

    @Select("""
            SELECT * FROM invoice_detail_tb
            WHERE id = #{invoiceId};
            """)
    @ResultMap("invoiceMap")
    Invoice getInvoiceById(Integer invoiceId);

    @Delete("""
            DELETE FROM invoice_tb
            WHERE invoice_id = #{invoiceId}
            """)
    void deleteInvoiceById(Integer invoiceId);

    @Select("""
            UPDATE invoice_tb 
            SET customer_id = #{invoice.customerId}
            WHERE invoice_id = #{invoiceId}    
            RETURNING *;      
            """)
    Integer updateInvoice(Integer invoiceId, @Param("invoice") InvoiceRequest invoiceRequest);

    @Select("""
            DELETE FROM invoice_detail_tb WHERE invoice_id = #{invoiceId};
            """)
    void deleteFromInvoiceDetail(Integer invoiceId);

    @Select("""
            INSERT INTO invoice_detail_tb (invoice_id, product_id)
            VALUES (#{invoiceId}, #{productId})
            """)
    void insertToInvoiceDetail(Integer invoiceId, Integer productId);


}
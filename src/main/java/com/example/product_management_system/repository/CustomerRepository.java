package com.example.product_management_system.repository;

import com.example.product_management_system.model.entity.Customer;
import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.CustomerRequest;
import com.example.product_management_system.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("""
            SELECT * FROM customer_tb
            """)
    @Results(id="customerMap", value = {
            @Result(property = "customerId" ,column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone"),
    })
    List<Customer> getAllCustomer();

    @Select("""
            SELECT * FROM customer_tb
            WHERE customer_id = #{customerId}
            """)
    @ResultMap("customerMap")
    Customer getCustomerById(Integer customerId);

    @Select("""
            INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
            VALUES (#{customerRequest.customerName} , #{customerRequest.customerAddress}, #{customerRequest.customerPhone})
            RETURNING *
            """)
    @ResultMap("customerMap")
    Customer insertCustomer(@Param("customerRequest") CustomerRequest customerRequest);

    @Select("""
            UPDATE customer_tb 
            SET customer_name = #{customerRequest.customerName}, 
            customer_address = #{customerRequest.customerAddress},
            customer_phone = #{customerRequest.customerPhone}
            WHERE customer_id = #{customerId} RETURNING *; 
            """)
    @ResultMap("customerMap")
    Customer updateCustomer(Integer customerId,@Param("customerRequest") CustomerRequest customerRequest);

    @Delete("""
            DELETE FROM customer_tb 
            WHERE customer_id = #{customerId}
            """)
    @ResultMap("customerMap")
    void deleteCustomerById(Integer customerId);
}

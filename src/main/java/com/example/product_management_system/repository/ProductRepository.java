package com.example.product_management_system.repository;

import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("""
            SELECT * FROM product_tb
            """)

    @Results(id="productMap", value = {
        @Result(property = "productId" ,column = "product_id"),
        @Result(property = "productName", column = "product_name"),
        @Result(property = "productPrice", column = "product_price"),
    })
    List<Product> getAllProduct();

    @Select("""
            SELECT * FROM product_tb
            WHERE product_id = #{productId}
            """)
    @ResultMap("productMap")
    Product getProductById(Integer productId);


    @Select("""
            INSERT INTO product_tb (product_name, product_price)
            VALUES (#{productRequest.productName} , #{productRequest.productPrice})
            RETURNING *
            """)
    @ResultMap("productMap")
    Product insertProduct(@Param("productRequest") ProductRequest productRequest);

    @Select("""
            UPDATE product_tb 
            SET product_name = #{productRequest.productName}, 
            product_price = #{productRequest.productPrice}
            WHERE product_id = #{productId} RETURNING *; 
            """)
    @ResultMap("productMap")
    Product updateProduct( Integer productId,@Param("productRequest") ProductRequest productRequest);

    @Delete("""
            DELETE FROM product_tb WHERE product_id = #{productId}
            """)
    @ResultMap("productMap")
    void deleteProduct(Integer productId);
}

package com.example.product_management_system.controller;

import com.example.product_management_system.model.entity.Customer;
import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.CustomerRequest;
import com.example.product_management_system.model.request.ProductRequest;
import com.example.product_management_system.model.response.ApiResponse;
import com.example.product_management_system.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/get-all-customer")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new ApiResponse<>(
                customerService.getAllCustomer(),
                "Successfully get all customer!",
                true
        ));
    }
    @GetMapping("/customer/get-customer-by-id/{id} ")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Integer customerId){
        Customer customer = customerService.getCustomerById(customerId);
        ApiResponse<Customer> response = new ApiResponse<>(
                customer,
                "Successfully get customer by Id!",
                true
        );
        return ResponseEntity.ok(response);
    }
    @PostMapping("/customer/add-new-customer")
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customerRequest){
        return ResponseEntity.ok(new ApiResponse<>(
                customerService.insertCustomer(customerRequest),
                "Successfully inserted customer",
                true
        ));
    }
    @PutMapping("/customer/update-customer-by-id/{id} ")
    public ResponseEntity<?> updateCustomer(@PathVariable("id") Integer customerId, @RequestBody CustomerRequest customerRequest ){

        return ResponseEntity.ok( new ApiResponse<>(
                customerService.updateCustomer(customerId, customerRequest),
                "Successfully update customer",
                true
        ));
    }
    @DeleteMapping("/customer/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable("id") Integer customerId ){
        customerService.deleteCustomerById(customerId);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "Successfully deleted product",
                true
        ));

    }
}

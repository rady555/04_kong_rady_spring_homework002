package com.example.product_management_system.controller;

import com.example.product_management_system.model.entity.Customer;
import com.example.product_management_system.model.entity.Invoice;
import com.example.product_management_system.model.request.CustomerRequest;
import com.example.product_management_system.model.request.InvoiceRequest;
import com.example.product_management_system.model.request.ProductRequest;
import com.example.product_management_system.model.response.ApiResponse;
import com.example.product_management_system.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class InvoiceController {
  private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/invoice/get-all-invoice")
    public ResponseEntity<?> getAllInvoice(){
        return ResponseEntity.ok(new ApiResponse<>(
                invoiceService.getAllInvoice(),
                "Successfully get all customer!",
                true
        ));
    }
    @PostMapping("/invoice/add-new-invoice")
    public ResponseEntity<?> insertInvoice(@RequestBody InvoiceRequest invoiceRequest){
        return ResponseEntity.ok(new ApiResponse<>(
                invoiceService.insertInvoice(invoiceRequest),
                "Successfully fetched product",
                true
        ));
    }
    @GetMapping("/invoice/get-invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable("id") Integer invoiceId){
        Invoice invoice =  invoiceService.getInvoiceById(invoiceId);
        ApiResponse<Invoice> response = new ApiResponse<>(
                invoice,
                "Successfully get invoice by Id!",
                true
        );
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/invoice/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable("id") Integer invoiceId){
        invoiceService.deleteCustomerById(invoiceId);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "Successfully deleted invoice",
                true
        ));
    }
    @PutMapping("/invoice/update-invoice-by-id/{id}")
    public ResponseEntity<?> updateInvoice(@PathVariable("id") Integer invoiceId, @RequestBody InvoiceRequest invoiceRequest ){

        return ResponseEntity.ok( new ApiResponse<>(
                invoiceService.updateInvoice(invoiceId,invoiceRequest),
                "Successfully updated product",
                true
        ));
    }
}

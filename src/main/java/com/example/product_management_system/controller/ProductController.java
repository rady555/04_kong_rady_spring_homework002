package com.example.product_management_system.controller;

import com.example.product_management_system.model.entity.Product;
import com.example.product_management_system.model.request.ProductRequest;
import com.example.product_management_system.model.response.ApiResponse;
import com.example.product_management_system.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1")

public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/get-all-product")
    public List<Product> getAllProduct(){
        return productService.getAllProduct();
    }

    @GetMapping("/product/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable("id") Integer productId){
        Product product = productService.getProductById(productId);
        ApiResponse<Product> response = new ApiResponse<>(
                product,
                "Successfully fetched product",
                true
        );

        return ResponseEntity.ok(response);
    }

    @PostMapping("/product/add-new-product")
    public ResponseEntity<?> insertProduct(@RequestBody ProductRequest productRequest){
        return ResponseEntity.ok(new ApiResponse<>(
                productService.insertProduct(productRequest),
                "Successfully fetched product",
                true
                ));

    }
    @PutMapping("/product/update-product-by-id/{id} ")
    public ResponseEntity<?> updateProduct(@PathVariable("id") Integer productId, @RequestBody ProductRequest productRequest ){

        return ResponseEntity.ok( new ApiResponse<>(
                productService.updateProduct(productId, productRequest),
                "Successfully fetched product",
                true
    ));
    }

    @DeleteMapping("/product/delete-product-by-id/{id} ")
    public ResponseEntity<?> deleteProductById(@PathVariable("id") Integer productId ){
       productService.deleteProductById(productId);
        return ResponseEntity.ok(new ApiResponse<>(
                null,
                "Successfully fetched product",
                true
        ));

    }
}
